package pl.com.kul.pesel.validation;

import java.util.regex.Pattern;

import static java.util.Objects.isNull;
import static pl.com.kul.pesel.validation.PeselValidationError.*;
import static pl.com.kul.pesel.validation.PeselValidationResult.validationFailed;

public class PeselValidator {

    private final static String ONLY_NUMBER_REGEX = Pattern
            .compile("^[0-9]*")
            .pattern();
    private final static int PESEL_LENGTH = 11;

    public PeselValidationResult validatePesel(String peselToValidate) {
        if (isNull(peselToValidate)) {
            return validationFailed(NULL_VALUE);
        }
        if (!validateLength(peselToValidate)) {
            return validationFailed(INCORRECT_LENGTH);
        }
        if (!validateCharacters(peselToValidate)) {
            return validationFailed(CONTAINS_ILLEGAL_CHARACTERS);
        }
        if (!validateChecksum(peselToValidate)) {
            return validationFailed(CHECKSUM_FAILED);
        }
        return PeselValidationResult.successfulValidation();
    }

    private boolean validateLength(String peselToValidate) {
        return peselToValidate.length() == PESEL_LENGTH;
    }

    private boolean validateCharacters(String peselToValidate) {
        return peselToValidate.matches(ONLY_NUMBER_REGEX);
    }

    //All code below this line should be extracted to another class. It's here only because of classes case.
    private boolean validateChecksum(String peselToValidate) {
        int[] peselDigits = extractPeselDigits(peselToValidate);
        int sumOfDigitsWeights = calculateSumOfWeights(peselDigits);
        int checksum = calculateChecksum(sumOfDigitsWeights);
        if (checksum == 10 && getChecksumPeselDigit(peselDigits) == '0') {
            return true;
        } else {
            return checksum == getChecksumPeselDigit(peselDigits);
        }
    }

    private int getChecksumPeselDigit(int[] peselDigits) {
        return peselDigits[10];
    }

    private int calculateChecksum(int sumOfDigitsWeights) {
        return 10 - (sumOfDigitsWeights % 10);
    }

    private int[] extractPeselDigits(String peselToValidate) {
        char[] chars = peselToValidate.toCharArray();
        int[] peselNumbers = new int[11];
        for(int i = 0 ; i<chars.length; i++){
            peselNumbers[i] = Integer.parseInt(String.valueOf(chars[i]));
        }
        return peselNumbers;
    }

    private int calculateSumOfWeights(int[] peselNumbers) {
        int sum = 0;
        sum += peselNumbers[0] * 1;
        sum += peselNumbers[1] * 3;
        sum += peselNumbers[2] * 7;
        sum += peselNumbers[3] * 9;
        sum += peselNumbers[4] * 1;
        sum += peselNumbers[5] * 3;
        sum += peselNumbers[6] * 7;
        sum += peselNumbers[7] * 9;
        sum += peselNumbers[8] * 1;
        sum += peselNumbers[9] * 3;
        return sum;
    }
}
