package pl.com.kul;

import pl.com.kul.pesel.validation.PeselValidationResult;
import pl.com.kul.pesel.validation.PeselValidator;

import java.time.LocalDate;

import static pl.com.kul.Gender.FEMALE;
import static pl.com.kul.Gender.MALE;

public class PeselDataExtractor {
    private final static int BIRTH_DATE_END_INDEX = 7;


    private final PeselValidator peselValidator;

    public PeselDataExtractor(PeselValidator peselValidator) {
        this.peselValidator = peselValidator;
    }

    public Gender extractGender(String pesel) {
        validatePesel(pesel);
        return pesel.charAt(9) % 2 == 0 ? FEMALE : MALE;
    }

    public LocalDate extractBirthDate(
            String pesel) { // brak walidacji poprawnosci daty - na potrzeby zajęć logiki wystarczy :)
        validatePesel(pesel);
        String birthDate = pesel.substring(0, BIRTH_DATE_END_INDEX);
        int daySignature = extract(PeselBirthDateParts.DAY, birthDate);
        int monthSignature = extract(PeselBirthDateParts.MONTH, birthDate);
        int yearSignature = extract(PeselBirthDateParts.YEAR, birthDate);

        return LocalDate.of(recalculateYear(yearSignature, monthSignature), recalculateMonth(monthSignature),
                daySignature);
    }

    private int recalculateMonth(int month) {
        return month % 20;
    }

    private int recalculateYear(int year, int month) {
        int centuryIndex = month / 20;
        switch (centuryIndex) {
            case 0:
                return 1900 + year;
            case 1:
                return 2000 + year;
            case 2:
                return 2100 + year;
            case 3:
                return 2200 + year;
            case 4:
                return 1800 + year;
            default:
                throw new IllegalStateException();
        }
    }

    private int extract(PeselBirthDateParts part, String birthDate) {
        return Integer.parseInt(birthDate.substring(part.getFirstIndex(), part.getSecondIndex() + 1));
    }

    private void validatePesel(String pesel) {
        PeselValidationResult peselValidationResult = peselValidator.validatePesel(pesel);
        if (!peselValidationResult.isPeselValid()) {
            throw new PeselValidationException("Pesel validation failed with message: " + peselValidationResult
                    .getValidationError()
                    .getMessage());
        }
    }


}
