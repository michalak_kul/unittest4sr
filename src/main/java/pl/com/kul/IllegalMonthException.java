package pl.com.kul;

public class IllegalMonthException extends RuntimeException {
    public IllegalMonthException(String message) {
        super(message);
    }
}
