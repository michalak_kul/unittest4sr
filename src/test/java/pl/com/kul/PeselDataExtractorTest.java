package pl.com.kul;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import pl.com.kul.pesel.validation.PeselValidationResult;
import pl.com.kul.pesel.validation.PeselValidator;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PeselDataExtractorTest {

    private PeselValidator peselValidator;
    private PeselDataExtractor peselDataExtractor;

    @BeforeEach
    void setUp() {
        peselValidator = Mockito.mock(PeselValidator.class);
        peselDataExtractor = new PeselDataExtractor(peselValidator);
    }

//    @Test
//    void shouldReturnCorrectGenderForCorrectPesel() {
//        //given
//        String femalePesel = "37042817943";
//        Gender expectedGender = Gender.FEMALE;
//        Mockito.when(peselValidator.validatePesel(femalePesel)).thenReturn(PeselValidationResult.successfulValidation());
//        //when
//        Gender result = peselDataExtractor.extractGender(femalePesel);
//        //then
//        Assertions.assertEquals(expectedGender, result);
//    }

    @ParameterizedTest
    @MethodSource(value = "peselDataExtractorSource")
    public void shouldTestAllPaths(String pesel, Gender expectedGender) {
        //given
        Mockito.when(peselValidator.validatePesel(pesel)).thenReturn(PeselValidationResult.successfulValidation());
        //when
        Gender result = peselDataExtractor.extractGender(pesel);
        //then
        assertEquals(expectedGender, result);
    }

    public static Stream<Arguments> peselDataExtractorSource() {
        return Stream.of(
                Arguments.of("37042817943", Gender.FEMALE),
                Arguments.of("18100322892", Gender.MALE)
        );
    }



}